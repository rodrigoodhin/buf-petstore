module gitlab.com/rodrigoodhin/buf-petstore

go 1.17

require (
	go.buf.build/library/go-grpc/rodrigoodhin/petapis v1.4.4
	google.golang.org/genproto v0.0.0-20211203200212-54befc351ae9
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	go.buf.build/library/go-grpc/rodrigoodhin/paymentapis v1.4.1 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.5 // indirect
)
